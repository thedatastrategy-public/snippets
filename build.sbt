name := "snippet"
organization := "com.tds"
scalaVersion := "2.11.12"
description := "Open Source Common Library for Textual Analysis in Spark"
homepage := Some(url("http://thedatastrategy.com"))
licenses := Seq("The Apache License, Version 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0.txt"))
scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/thedatastrategy-public/snippets"),
    "https://gitlab.com/thedatastrategy-public/snippets.git")
)
developers := List(
  Developer(
    id = "arj-196",
    name = "arj un",
    email = "arjruler93@gmail.com",
    url = url("https://gitlab.com/arj-196")
  )
)
pomIncludeRepository := { _ => false }
publishMavenStyle := true
publishArtifact in Test := false
publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value)
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "service/local/staging/deploy/maven2")
}


/*
  Dependencies
 */

// spark
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.1.0"
libraryDependencies += "org.apache.spark" %% "spark-mllib" % "2.1.0"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.1.0"
libraryDependencies += "org.apache.spark" %% "spark-streaming" % "2.1.0"

// spark elasticsearch
libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "6.1.1"

// opennlp
libraryDependencies += "org.apache.opennlp" % "opennlp-tools" % "1.7.2"

// deeplearning4j
val deeplearning4jVersion = "0.9.1"
libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" % deeplearning4jVersion
libraryDependencies += "org.deeplearning4j" % "deeplearning4j-nlp" % deeplearning4jVersion
libraryDependencies += "org.nd4j" % "nd4j-native-platform" % deeplearning4jVersion

// weka general purpose machine learning
libraryDependencies += "nz.ac.waikato.cms.weka" % "weka-stable" % "3.8.1"

// string similarity metrics
libraryDependencies += "info.debatty" % "java-string-similarity" % "1.0.0"

// kafka streams
libraryDependencies += "org.apache.kafka" % "kafka-streams" % "0.11.0.0"

// loggers from kafka stream tutorial
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.25"
libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.25"

// leverage java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")
scalacOptions := Seq("-target:jvm-1.8")
initialize := {
  val _ = initialize.value
  if (sys.props("java.specification.version") != "1.8")
    sys.error("Java 8 is required for this project.")
}

// akka actor
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.5.8"
// akka http
//libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.0.11"
libraryDependencies += "com.typesafe.akka" %% "akka-http-experimental" % "2.4.11.2"

// akka persistence
libraryDependencies += "com.typesafe.akka" %% "akka-persistence" % "2.5.8"
libraryDependencies += "org.iq80.leveldb" % "leveldb" % "0.10"
libraryDependencies += "org.fusesource.leveldbjni" % "leveldbjni-all" % "1.8"

// socket io
libraryDependencies += "io.scalecube" % "socketio" % "2.3.4"
libraryDependencies += "io.netty" % "netty-buffer" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-common" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-handler" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-codec" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-codec-http" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-transport" % "4.1.6.Final"
libraryDependencies += "io.netty" % "netty-transport-native-epoll" % "4.1.6.Final"
libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.22"

//<classifier>linux-x86_64</classifier>
