const WebSocket = require('ws');

const wss = new WebSocket.Server({port: 8080});
console.log("Starting Websocket Server");

wss.on('connection', function connection(ws) {
    ws.on('message', function incoming(message) {
        console.log('received: %s', message);
    });

    for (var i=0; i < 1000; i ++) {
        ws.send('something=' + i);
    }
});

