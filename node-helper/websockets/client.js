const WebSocket = require('ws');

console.log("starting websocket client")
const ws = new WebSocket('ws://localhost:8080/ws-echo', {
    perMessageDeflate: false
});

ws.on('open', function open() {
    console.log('sending something');
    ws.send('something');
});

ws.on('message', function incoming(data) {
    console.log('received', data);
});
