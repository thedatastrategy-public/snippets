package com.tds.snippet.akka

import akka.actor.{Actor, ActorSystem, Props}
import com.tds.snippet.akka.MusicController.{Play, Stop}
import com.tds.snippet.akka.MusicPlayer.{PlayMusic, StopMusic}

// music controller
object MusicController {
  sealed trait ControllerMsg
  case object Play extends ControllerMsg
  case object Stop extends ControllerMsg

  def props = Props[MusicController]
}

class MusicController extends Actor {
  override def receive: Receive = {
    case Play => println("Music Started")
    case Stop => println("Music Stopped")
  }
}

// music player
object MusicPlayer {
  sealed trait PlayerMsg
  case object PlayMusic extends PlayerMsg
  case object StopMusic extends PlayerMsg
}

class MusicPlayer extends Actor {
  override def receive: Receive = {
    case StopMusic => println(" I dont want to stop music")
    case PlayMusic =>
      val controller = context.actorOf(MusicController.props, "controller")
      controller ! Play
    case _ => println("unknown message")
  }
}

object Creation extends App {
  val system = ActorSystem("creation")
  // music player
   val player = system.actorOf(Props[MusicPlayer], "player")
  player  ! PlayMusic
}