import UserStorage.{Connect, DBOperation, DisConnect}
import akka.actor.{Actor, ActorSystem, Props, Stash}

object UserStorage {
  sealed trait DBOperationMsg

  case object Connect extends DBOperationMsg
  case object DisConnect extends DBOperationMsg
  case object DBOperation extends DBOperationMsg
}

class UserStorage extends Actor with Stash {
  override def receive: Receive = disConnect

  def disConnect: Actor.Receive = {
    case DBOperation =>
      println("Disconnect State: Received DBOperation, Stashing message")
      stash()
    case Connect =>
      println("Disconnect State: Received Connect Command, Connecting to DB")
      println("Disconnect State: Unstashing all messages")
      unstashAll()
      context.become(connect)
    case DisConnect =>
      println("Disconnect State: Received Disconnect Command, Already disconnected")
  }

  def connect: Actor.Receive = {
    case DBOperation =>
      println("Connect State: Received DBOperation, Performing Operation")
    case Connect =>
      println("Connect State: Received Connect Command, Already Connected")
    case DisConnect =>
      println("Connect State: Received Disconnect Command, Disconnecting from DB")
      context.unbecome()
  }
}


object ChangeBehavior extends App {
  val system = ActorSystem("ChangeBehavior")
  val userStorage = system.actorOf(Props[UserStorage])
  userStorage ! DBOperation
  userStorage ! DBOperation
  userStorage ! Connect
  userStorage ! DBOperation
  userStorage ! DisConnect
  userStorage ! DBOperation
  userStorage ! DBOperation
  userStorage ! Connect
  userStorage ! DBOperation
  userStorage ! DisConnect

  Thread.sleep(100)
  system.terminate()
}