package com.tds.snippet.akka

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import com.tds.snippet.akka.Checker.{BlackUser, CheckUser, WhiteUser}
import com.tds.snippet.akka.Recorder.NewUser
import com.tds.snippet.akka.Storage.AddUser
import akka.util.Timeout

import scala.concurrent.duration._
case class User(username: String,  email: String)
import akka.pattern.ask

object Recorder {
  sealed trait RecorderMsg
  case class NewUser(user: User) extends RecorderMsg
}

object Checker {
  sealed trait CheckerMsg
  case class CheckUser(user: User) extends CheckerMsg

  sealed trait CheckerResponse
  case class BlackUser(user: User) extends CheckerResponse
  case class WhiteUser(user: User) extends CheckerResponse
}

object Storage {
  sealed trait StorageMsg
  case class AddUser(user: User) extends StorageMsg
}

class Storage extends Actor {
  var users = List.empty[User]

  override def receive: Receive = {
    case AddUser(user) =>
      println(s"Storage: adding user $user")
      users = user :: users
  }
}

class Checker extends Actor {
  val blackList = List(
    User("Adam", "adam@gmail.com")
  )

  override def receive: Receive = {
    case CheckUser(user) if blackList.contains(user) =>
      println(s"Checker: $user is in blacklist")
      sender() ! BlackUser(user)
    case CheckUser(user) =>
      println(s"Checker: $user is in whitelist")
      sender() ! WhiteUser(user)
  }
}

class Recorder(checker: ActorRef, storage: ActorRef) extends Actor {
  import scala.concurrent.ExecutionContext.Implicits.global
  implicit val timeout = Timeout(5 seconds)

  override def receive: Receive = {
    case NewUser(user) =>
      println("Recorder: Received Message NewUser")
      checker ? CheckUser(user) map {
        case WhiteUser(user) =>
          storage ! AddUser(user)
        case BlackUser(user) =>
          println(s"Recorder: Not Adding User $user")
      }
  }
}

object UserRegistry extends App {
  val system = ActorSystem("UserRegistry")

  val storage = system.actorOf(Props[Storage])
  val checker = system.actorOf(Props[Checker])
  val recorder = system.actorOf(Props(new Recorder(checker, storage)))

  recorder ! NewUser(User("Jon", "jon@gmail.com"))
  recorder ! NewUser(User("Adam", "adam@gmail.com"))
  recorder ! NewUser(User("Jon", "jon@gmail.com"))
  recorder ! NewUser(User("Adam", "adam@gmail.com"))
  recorder ! NewUser(User("Jon", "jon@gmail.com"))
  recorder ! NewUser(User("Adam", "adam@gmail.com"))
  recorder ! NewUser(User("Jon", "jon@gmail.com"))
  recorder ! NewUser(User("Adam", "adam@gmail.com"))
  recorder ! NewUser(User("Jon", "jon@gmail.com"))
  recorder ! NewUser(User("Adam", "adam@gmail.com"))

  Thread.sleep(1000)

  system.terminate()
}