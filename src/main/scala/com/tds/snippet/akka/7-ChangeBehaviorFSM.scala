package com.tds.snippet.akka

import UserStorage._
import akka.actor.{Actor, ActorSystem, FSM, Props, Stash}

object UserStorage {
  // FSM State
  sealed trait State
  case object Connected extends State
  case object DisConnected extends State

  // FSM Data
  sealed trait Data
  case object EmptyData extends Data

  sealed trait DBOperationMsg
  case object Connect extends DBOperationMsg
  case object DisConnect extends DBOperationMsg
  case object DBOperation extends DBOperationMsg
}

class UserStorage extends FSM[UserStorage.State, UserStorage.Data] with Stash {

  startWith(DisConnected, EmptyData)

  when(DisConnected) {
    case Event(DBOperation, _) =>
      println("Disconnect State: Received DBOperation, Stashing message")
      stash()
      stay() using EmptyData
    case Event(Connect, _) =>
      println("Disconnect State: Received Connect Command, Connecting to DB")
      println("Disconnect State: Unstashing all messages")
      unstashAll()
      goto(Connected) using EmptyData
    case Event(DisConnect, _) =>
      println("Disconnect State: Received Disconnect Command, Already disconnected")
      stay() using EmptyData
  }

  when(Connected) {
    case Event(DBOperation, _) =>
      println("Connect State: Received DBOperation, Performing Operation")
      stay using EmptyData
    case Event(Connect, _) =>
      println("Connect State: Received Connect Command, Already Connected")
      stay using EmptyData
    case Event(DisConnect, _) =>
      println("Connect State: Received Disconnect Command, Disconnecting from DB")
      goto(DisConnected) using EmptyData
  }

}


object ChangeBehaviorFSM extends App {
  val system = ActorSystem("ChangeBehavior")
  val userStorage = system.actorOf(Props[UserStorage])
  userStorage ! DBOperation
  userStorage ! DBOperation
  userStorage ! Connect
  userStorage ! DBOperation
  userStorage ! DisConnect
  userStorage ! DBOperation
  userStorage ! DBOperation
  userStorage ! Connect
  userStorage ! DBOperation
  userStorage ! DisConnect

  Thread.sleep(100)
  system.terminate()
}