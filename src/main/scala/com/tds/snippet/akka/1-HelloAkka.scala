package com.tds.snippet.akka

import akka.actor.{Actor, ActorRef, ActorSystem, Props}

// actor message
case class WhoToGreet(who: String)

// greeter actor
class Greeter extends Actor {
  override def receive: Receive = {
    case WhoToGreet(who) => println(s"Hello $who")
  }
}

object HelloAkkaScala extends App {

  // create actor system
  val system = ActorSystem("Hello-Akka")
  // create the greeter actor
  val greeter: ActorRef = system.actorOf(Props[Greeter], "greeter")
  // send message to greater
  greeter ! WhoToGreet("Akka ")

}
