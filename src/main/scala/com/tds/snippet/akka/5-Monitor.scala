
import akka.actor.{ActorRef, ActorSystem, Props, Actor, Terminated}

class Ares(athena: ActorRef) extends Actor {

  override def preStart(): Unit = {
    context.watch(athena)
  }

  override def postStop(): Unit = {
    println("Ares postStop")
  }

  override def receive: Receive = {
    case Terminated(_) =>
      println("Ares received terminate")
      context.stop(self)
  }
}

class Athena extends Actor {
  override def receive: Receive = {
    case msg =>
      println(s"Athena received msg: $msg")
      context.stop(self)
  }
}

object Monitor extends App {
  val system = ActorSystem("Monitor")
  val athena = system.actorOf(Props[Athena])
  val ares = system.actorOf(Props(classOf[Ares], athena))

  athena ! "Hi"
  Thread.sleep(100)
  system.terminate()
}
