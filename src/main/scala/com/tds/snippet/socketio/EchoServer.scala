package com.tds.snippet.socketio

import io.netty.util.CharsetUtil
import io.scalecube.socketio.Session

object EchoServer extends App {

  import io.netty.buffer.ByteBuf
  import io.scalecube.socketio.SocketIOListener
  import io.scalecube.socketio.SocketIOServer

  val logServer = SocketIOServer.newInstance(8080)
  logServer.setListener(new SocketIOListener() {
    def onConnect(session: Session): Unit = {
      System.out.println("Connected: " + session)
    }

    def onMessage(session: Session, message: ByteBuf): Unit = {
      System.out.println("Received: " + message.toString(CharsetUtil.UTF_8))
      message.release
    }

    def onDisconnect(session: Session): Unit = {
      System.out.println("Disconnected: " + session)
    }
  })

  logServer.start()
}
